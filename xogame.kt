import java.util.*

class Game(var x:Int, var o:Int,var draw:Int){
    companion object{
        var turn = "x"
        fun switchTurn(){
            if(Game.turn=="x"){
                Game.turn = "o"
            }else{
                Game.turn = "x"
            }
        }
    }
    fun xWin():Int{
        x = x.plus(1)
        return x
    }
    fun oWin():Int{
        o = o.plus(1)
        return o
    }
    fun gameDraw():Int{
        draw = draw.plus(1)
        return draw
    }
    fun showResult(){
        println("x won ${x} times , o won ${o} times , draw ${draw} times")
    }
}

class Table(){
    companion object{
        var table = createTable()
        fun createTable():Array<Array<String>> {
            var row1 = arrayOf("-", "-", "-")
            var row2 = arrayOf("-", "-", "-")
            var row3 = arrayOf("-", "-", "-")

            var _table = arrayOf(row1, row2, row3)

            return _table
        }

        fun updateTable(row:Int,col:Int):Boolean{

            for ( r in table){
                for ((index) in table.withIndex()){
                    if(table.indexOf(r)==row.dec() && index==col.dec()) {
                        if(!(table[table.indexOf(r)][index] == "-")){
                            println("Error : ${row} and ${col} exist")
                            return false
                        }else {
                            table[table.indexOf(r)][index] = Game.turn
                            showTable(table)
                            Game.switchTurn()
                            return true
                        }
                    }
                    //("${table[table.indexOf(r)][index]}")
                }
            }
            return false
        }

        fun haveWinner(game:Game):Boolean{
            for ( r in table){
                if(table[table.indexOf(r)][0] == table[table.indexOf(r)][1] && table[table.indexOf(r)][0] == table[table.indexOf(r)][2] && table[table.indexOf(r)][0]!="-"){
                    whoWinner(table[table.indexOf(r)][0],game)
                    return true
                }else if(table[0][table.indexOf(r)] == table[1][table.indexOf(r)] && table[0][table.indexOf(r)] == table[2][table.indexOf(r)] && table[0][table.indexOf(r)]!="-"){
                    whoWinner(table[0][table.indexOf(r)],game)
                    return true
                }else if((table[0][0] == table[1][1] && table[0][0] == table[2][2]) || (table[2][0] == table[1][1] && table[2][0] == table[0][2]) && table[1][1]!="-"){
                    whoWinner(table[1][1],game)
                    return true
                }
            }
            return false
        }

        fun whoWinner(winner:String,game:Game){
            if(winner=="x"){
                game.xWin()
            }else{
                game.oWin()
            }
            println("${winner} is winner !!")
        }

        fun showTable(tables:Array<Array<String>>){
            println("  1 2 3")
            for ( r in tables){
                print(tables.indexOf(r).inc())
                for ( c in r ){
                    print(" ${c}")
                }
                println()
            }
        }
    }
}

fun playagain():Boolean{
    println("Play again ? ( Y or N )")
    val reader = Scanner(System.`in`)
    var ans: String = reader.next()
    if(ans=="Y"){
        Game.turn = "x"
        return true
    }
    return false
}

fun playing(){
    val reader = Scanner(System.`in`)
    while(true){
        println("This is ${Game.turn} turn")
        print("Enter your row and column  : ")
        var row: Int = reader.nextInt()
        var col: Int = reader.nextInt()
        if(inputError(row,col)){
            continue
        }
        println("===============================================")
        if(Table.updateTable(row,col)){
            return
        }else{
            playing()
        }
    }
}

fun inputError(row:Int,col:Int):Boolean{
    if((row < 1 || row > 3 ) || (col < 1 || col > 3)){
        println("Error row and column must be number between 1 to 3")
        println("===============================================")
        return true
    }
    return false
}

fun main(args: Array<String>) {
    var game = Game(0,0,0)
    println("Welcome to my XO game")
    while(true) {
        println("Game started !")
        for(i in 0..8){
            playing()
            if(Table.haveWinner(game)){
                break
            }
            else if(i==8){
                game.gameDraw()
            }
        }
        game.showResult()
        if(playagain()){
            //break
            Table.table = Table.createTable()
        }
    }
    println("Thank for playing :)")
}